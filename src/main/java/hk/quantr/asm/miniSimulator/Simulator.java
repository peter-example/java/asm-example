/*
 * Copyright 2022 Stupid Genius.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.asm.miniSimulator;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

//example from https://gist.github.com/norswap/c7107e26806189d46a3feff6bc529d1f
//youtube link: https://www.youtube.com/watch?v=HqkH3nft2n0
public class Simulator {

	ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
	private void add() {
		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, "add", "(III)V", null, null);
		mv.visitCode();
		mv.visitVarInsn(Opcodes.ILOAD, 1);
		mv.visitVarInsn(Opcodes.ILOAD, 2);
		mv.visitInsn(Opcodes.IADD);
		mv.visitVarInsn(Opcodes.ISTORE, 0);
		mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mv.visitVarInsn(Opcodes.ILOAD, 1);
		mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);
		mv.visitInsn(Opcodes.RETURN);
		mv.visitMaxs(-1, -1);
		mv.visitEnd();
	}
	
	private void add(int x, int y){
		x=x+y;
		
	}
	public byte[] serializeToBytes() {
		cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "Simulator", null, "java/lang/Object", null);
		add();
		cw.visitEnd();
		return cw.toByteArray();
	}
}



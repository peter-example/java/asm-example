/*
 * Copyright 2022 darre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.asm.miniSimulator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

public class main {
	public static void main(String[] args) {
		Simulator sim = new Simulator();
		byte[] b = sim.serializeToBytes();
		Loader loader = new Loader();
		try {
			FileUtils.writeByteArrayToFile(new File("Sim.class"), b);
			loader.defineClass("Simulator", b).getMethod("add", String[].class).invoke(null, new Object[]{null});
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			System.out.println("No such instruction!");
		} catch (IOException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}

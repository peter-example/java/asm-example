package hk.quantr.asm.miniSimulator;

public class Loader extends ClassLoader {
	static { registerAsParallelCapable(); }
	public static final Loader INSTANCE = new Loader();
	public Class<?> defineClass(String binaryName, byte[] bytecode) {
		return defineClass(binaryName, bytecode, 0, bytecode.length);
	}
}
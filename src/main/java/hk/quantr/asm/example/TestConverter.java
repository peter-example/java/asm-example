package hk.quantr.asm.example;

import hk.quantr.asm.miniSimulator.Loader;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

/*
 * Copyright 2022 darre.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author darre
 */
public class TestConverter {

	public TestConverter() throws Exception {
		ClassReader cr = new ClassReader("hk.quantr.asm.example.AsmExample");
		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		Adapter ca = new Adapter(cw);
		cr.accept(ca, 0);
		byte[] b = cw.toByteArray();
		FileUtils.writeByteArrayToFile(new File("AsmExample.class"), b);
		Loader l = new Loader();
		Class c = l.defineClass("hk.quantr.asm.example.AsmExample", b);
//		Object instance = c.getDeclaredConstructor().newInstance();
		c.getMethod("main", String[].class).invoke(null, new Object[]{null});
		
//		System.out.println(CommonLib.getHexString(b, ", "));
	}

	public static void main(String[] args) throws Exception {

		new TestConverter();

	}

}

package hk.quantr.asm.example;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AdviceAdapter;

public class ChangeAdapter extends AdviceAdapter {

	private String methodName = null;

	public ChangeAdapter(MethodVisitor mv, int access, String name, String desc) {
		super(Opcodes.ASM5, mv, access, name, desc);
		methodName = name;
	}

	@Override
	protected void onMethodEnter() {
		super.onMethodEnter();
		System.out.println("hi");
		mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mv.visitLdcInsn("entering");
		mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
	}

	@Override
	protected void onMethodExit(int opcode) {
		super.onMethodExit(opcode);
		System.out.println("bye");
		mv.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mv.visitLdcInsn("bye");
		mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
		System.out.println(opcode);
	}

	@Override
	public void visitMaxs(int i, int i1) {
		super.visitMaxs(i, i1);
	}
}

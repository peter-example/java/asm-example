package hk.quantr.asm.example;
//

public class AsmExample {
	public long[] registers = new long[32];
	
	public void add(int a, int b, int c) {
		registers[3] = registers[1] + registers[2];
	}
	
	public static void main(String[] args) {
		AsmExample a = new AsmExample();
		a.add(3, 1, 2);
		System.out.println(a.registers[3]);
	}
	
	public void block() {
		
	}
}


import hk.quantr.javalib.CommonLib;
import org.junit.Test;
import org.objectweb.asm.ClassWriter;
import static org.objectweb.asm.Opcodes.ACC_ABSTRACT;
import static org.objectweb.asm.Opcodes.ACC_INTERFACE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.V1_5;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGenerateClass {

	@Test
	public void test() {
		ClassWriter cw = new ClassWriter(0);
		cw.visit(V1_5, ACC_PUBLIC + ACC_ABSTRACT + ACC_INTERFACE, "hk/quantr/Test1", null, "java/lang/Object", null);
		cw.visitEnd();
		byte[] b = cw.toByteArray();
		System.out.println(CommonLib.getHexString(b, ", "));

		MyClassLoader myClassLoader = new MyClassLoader();
		Class c = myClassLoader.defineClass("hk.quantr.Test1", b);
	}

	class MyClassLoader extends ClassLoader {

		public Class defineClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}
}


import java.io.IOException;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.ASM9;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestClassPrinter {

	@Test
	public void test() throws IOException {
		System.out.println("JRE version is " + System.getProperty("java.version"));
		MyClassPrinter cp = new MyClassPrinter();
//		ClassReader cr = new ClassReader("TestClassPrinter");
		ClassReader cr = new ClassReader("java.lang.Runnable");
//		ClassReader cr = new ClassReader(getClass().getResourceAsStream("java/lang/Runnable.class"));
		cr.accept(cp, 0);
	}

	class MyClassPrinter extends ClassVisitor {

		public MyClassPrinter() {
			super(ASM9);
		}

		@Override
		public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
			System.out.println("> "+name + " extends" + superName + " {");
		}

		@Override
		public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
			System.out.println(">     " + desc + " " + name);
			return null;
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			System.out.println(">     " + name + desc);
			return null;
		}

		@Override
		public void visitEnd() {
			System.out.println("> }");
		}
	}
}

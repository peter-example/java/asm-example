
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.ISTORE;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V1_8;

/**
 *
 * @author Peter
 */
public class TestOneMillionAddition {

	static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:s.S");

	ClassWriter cw = new ClassWriter(0);

	@Test
	public void test() throws InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		System.out.println(sdf.format(new Date()) + " Generating Yin.class");
		byte[] b = serializeToBytes("Yin");
		MyClassLoader myClassLoader = new MyClassLoader();
		Class c = myClassLoader.defineClass("Yin", b);
		Object[] arguments = new Object[]{null};
		Method mainMethod = c.getMethod("main", String[].class);
		System.out.println(sdf.format(new Date()) + " Start Yin.class");
		mainMethod.invoke(null, arguments);
		System.out.println(sdf.format(new Date()) + " End Yin.class");
	}

	class MyClassLoader extends ClassLoader {

		public Class defineClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}

	public byte[] serializeToBytes(String outputClazzName) {
		cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, outputClazzName, null, "java/lang/Object", null);
		addStandardConstructor();
		addMainMethod();
		cw.visitEnd();
		return cw.toByteArray();
	}

	void addStandardConstructor() {
		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
		mv.visitInsn(RETURN);
		mv.visitMaxs(100, 100);
		mv.visitEnd();
	}

	void addMainMethod() {
		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
		mv.visitCode();
		mv.visitInsn(ICONST_1);
		mv.visitVarInsn(ISTORE, 1);
		for (long x = 0; x < 10000; x++) {
			mv.visitInsn(ICONST_1);
			mv.visitVarInsn(ISTORE, 2);
			mv.visitVarInsn(ILOAD, 1);
			mv.visitVarInsn(ILOAD, 2);
			mv.visitInsn(IADD);
			mv.visitVarInsn(ISTORE, 1);
		}
		mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mv.visitVarInsn(ILOAD, 1);
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);
		mv.visitInsn(RETURN);
		mv.visitMaxs(100, 100);
		mv.visitEnd();
	}
}


import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.ISTORE;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V1_8;

/*
 * Copyright 2022 yin.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author yin
 */
public class TestGenerateAddtionAndRunAndSetBreakpintAndSuspend {

	ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);

	@Test
	public void test() throws InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException, InterruptedException {
		List<VirtualMachineDescriptor> virtualMachineDescriptors = VirtualMachine.list();
		for (VirtualMachineDescriptor descriptor : virtualMachineDescriptors) {
			System.out.println("> " + descriptor.displayName());

			VirtualMachine virtualMachine = null;
			try {
				virtualMachine = VirtualMachine.attach(descriptor);
				virtualMachine.loadAgent("{agent}", "{params}");
			} catch (AgentInitializationException | AgentLoadException | AttachNotSupportedException | IOException e) {
				if (virtualMachine != null) {
					virtualMachine.detach();
				}
			}
			break;
		}

		byte[] b = serializeToBytes("Yin");
		FileUtils.writeByteArrayToFile(new File("Yin.class"), b);
		MyClassLoader myClassLoader = new MyClassLoader();
		Class c = myClassLoader.defineClass("Yin", b);
		Object[] arguments = new Object[]{null};
		Method mainMethod = c.getMethod("main", String[].class);
		mainMethod.invoke(null, arguments);

//		Thread t1 = new Thread() {
//			@Override
//			public void run() {
//				try {
//					mainMethod.invoke(null, arguments);
//				} catch (IllegalAccessException | InvocationTargetException ex) {
//					Logger.getLogger(TestGenerateAddtionAndRunAndSuspend.class.getName()).log(Level.SEVERE, null, ex);
//				}
//			}
//		};
		InputStreamReader inputStreamReader = new InputStreamReader(System.in);
		BufferedReader reader = new BufferedReader(inputStreamReader);
//		t1.start();
//		while (true) {
//			System.out.println(">>");
//			String line = reader.readLine();
//			System.out.println("line=" + line);
//			if (line.equals("q")) {
//				break;
//			} else {
//				t1.suspend();
//			}
//		}
	}

	class MyClassLoader extends ClassLoader {

		public Class defineClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}

	public byte[] serializeToBytes(String outputClazzName) {
		cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, outputClazzName, null, "java/lang/Object", null);
		addStandardConstructor();
		addMainMethod();
		cw.visitEnd();
		return cw.toByteArray();
	}

	void addStandardConstructor() { 
		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
		mv.visitInsn(RETURN);
		mv.visitMaxs(100, 100);
		mv.visitEnd();
	}

	void addMainMethod() {
		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, null);
		mv.visitCode();
		mv.visitInsn(ICONST_1);
		mv.visitVarInsn(ISTORE, 1);
		mv.visitInsn(ICONST_1);
		mv.visitVarInsn(ISTORE, 2);
		Label l1 = new Label();
		mv.visitLabel(l1);
		mv.visitVarInsn(ILOAD, 1);
		mv.visitVarInsn(ILOAD, 2);
		mv.visitInsn(IADD);
		mv.visitVarInsn(ISTORE, 2);
		mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mv.visitVarInsn(ILOAD, 2);
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);
		mv.visitJumpInsn(GOTO, l1);
		mv.visitInsn(RETURN);
		mv.visitMaxs(100, 100);
		mv.visitEnd();

	}
}
